package auth

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"strconv"
	"strings"

	"gitlab.com/lmendes_94/spotifyauth/config"
	"gitlab.com/lmendes_94/spotifyauth/server"
	httpres "gitlab.com/lmendes_94/spotifyauth/server/http"
	"gitlab.com/lmendes_94/spotifyauth/server/middlewares"
)

const (
	spotifyLoginBaseUrl = "https://accounts.spotify.com"
	spotifyApiBaseUrl   = "https://api.spotify.com"
)

type authController struct {
	config *config.Config
	client *http.Client
}

func (a *authController) createRedirectUri() string {
	return fmt.Sprintf("http://%s:%d/api/callback", a.config.BindHost, a.config.BindPort)
}

func (a *authController) authenticate(w http.ResponseWriter, r *http.Request) {
	var showDialog bool

	if show, ok := r.URL.Query()["show_dialog"]; ok {
		showDialog = strings.ToLower(show[0]) == "true"
	}

	redirectUri := a.createRedirectUri()

	authQueryParams := url.Values{
		"response_type": {"code"},
		"client_id":     {a.config.SpotifyClientId},
		"redirect_uri":  {redirectUri},
		"show_dialog":   {strconv.FormatBool(showDialog)},
	}

	if a.config.SpotifyScopes != "" {
		authQueryParams["scopes"] = strings.Split(a.config.SpotifyScopes, ",")
	}

	url := fmt.Sprintf("%s/authorize?%s", spotifyLoginBaseUrl, authQueryParams.Encode())
	http.Redirect(w, r, url, http.StatusMovedPermanently)
}

func (a *authController) getBearerToken(w http.ResponseWriter, r *http.Request) {
	querystring := r.URL.Query()
	redirectUri := a.createRedirectUri()

	authPayload := url.Values{
		"grant_type":   {"authorization_code"},
		"code":         {querystring.Get("code")},
		"redirect_uri": {redirectUri},
	}

	a.makeTokenRequest(w, authPayload)
}

func (a *authController) refreshToken(w http.ResponseWriter, r *http.Request) {
	querystring := r.URL.Query()
	authPayload := url.Values{
		"grant_type":    {"refresh_token"},
		"refresh_token": {querystring.Get("refresh_token")},
	}

	a.makeTokenRequest(w, authPayload)
}

func (a *authController) getCurrentUserData(w http.ResponseWriter, r *http.Request) {
	currentUserUrl := fmt.Sprintf("%s/v1/me", spotifyApiBaseUrl)

	request, _ := http.NewRequest(middlewares.RequestTypeGet, currentUserUrl, nil)
	request.Header.Set("Authorization", r.Header.Get("Authotization"))
	response, err := a.client.Do(request)

	if err != nil {
		httpres.Create(http.StatusInternalServerError, w, httpres.SpotifyResponsePayload{
			"message": err.Error(),
		})
		return
	}

	defer response.Body.Close()
	var resPayload httpres.SpotifyResponsePayload

	json.NewDecoder(response.Body).Decode(&resPayload)
	httpres.Create(response.StatusCode, w, resPayload)
}

func (a *authController) makeTokenRequest(w http.ResponseWriter, values url.Values) {
	authData := fmt.Sprintf("%s:%s", a.config.SpotifyClientId, a.config.SpotifyClientSecret)
	b64Data := base64.StdEncoding.EncodeToString([]byte(authData))

	authUrl := fmt.Sprintf("%s/api/token", spotifyLoginBaseUrl)

	request, _ := http.NewRequest(middlewares.RequestTypePost, authUrl, strings.NewReader(values.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	request.Header.Set("Authorization", fmt.Sprintf("Basic %s", b64Data))

	response, err := a.client.Do(request)

	if err != nil {
		httpres.Create(http.StatusInternalServerError, w, httpres.SpotifyResponsePayload{
			"message": err.Error(),
		})
		return
	}

	defer response.Body.Close()
	var resPayload httpres.SpotifyResponsePayload

	json.NewDecoder(response.Body).Decode(&resPayload)
	httpres.Create(response.StatusCode, w, resPayload)
}

func (a *authController) Register() {
	http.HandleFunc("/api/login/", middlewares.HttpRoute(a.authenticate, middlewares.RequestTypeGet))
	http.HandleFunc("/api/refresh/", middlewares.HttpRoute(a.refreshToken, middlewares.RequestTypeGet))
	http.HandleFunc("/api/callback/", middlewares.HttpRoute(a.getBearerToken, middlewares.RequestTypeGet))
	http.HandleFunc("/api/me/", middlewares.HttpRouteAuthenticated(a.getCurrentUserData, middlewares.RequestTypeGet))
}

func New(config *config.Config) server.RouteHandler {
	return &authController{
		config: config,
		client: &http.Client{},
	}
}
