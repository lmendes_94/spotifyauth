package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"gitlab.com/lmendes_94/spotifyauth/auth"
	"gitlab.com/lmendes_94/spotifyauth/config"
	"gitlab.com/lmendes_94/spotifyauth/server"
)

var (
	bindPort      int
	serverTimeout int
	bindHost      string
	clientId      string
	clientSecret  string
	scopes        string
)

func getEnvOrFlag(enviromentVarName, flagToCheck string) string {
	if flagToCheck == "" {
		return os.Getenv(enviromentVarName)
	}

	return flagToCheck
}

func init() {
	flag.IntVar(&bindPort, "port", 5500, "set server port")
	flag.StringVar(&bindHost, "host", "localhost", "set desierd host name")
	flag.StringVar(&clientId, "client-id", "", "set spotify client_id")
	flag.StringVar(&clientSecret, "client-secret", "", "set spotify client_secret")
	flag.StringVar(&scopes, "scopes", "user-read-private,user-read-email", "set spotify api scopes")
	flag.IntVar(&serverTimeout, "timeout", 5, "timeout shutdown server")
}

func main() {
	flag.Parse()

	config, err := config.New(
		bindPort,
		bindHost,
		getEnvOrFlag("SPOTIFY_CLIENT_ID", clientId),
		getEnvOrFlag("SPOTIFY_CLIENT_SECRET", clientSecret),
		scopes,
	)

	if err != nil {
		fmt.Fprintf(os.Stderr, "error %v", err)
		os.Exit(1)
	}

	server := server.New(
		config,
		auth.New(config),
	)

	go server.Start()
	log.Printf("server listen on port %d\n", config.BindPort)

	var signalChan = make(chan os.Signal)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	<-signalChan

	log.Println("Shutting down server...")

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(serverTimeout))
	defer cancel()

	if err := server.ShutdownServer(ctx); err != nil {
		log.Fatal("Server forced to shutdown: ", err)
	}

	log.Println("Server exiting...")
}
