package config

import "errors"

type Config struct {
	BindPort            int
	BindHost            string
	SpotifyClientId     string
	SpotifyClientSecret string
	SpotifyScopes       string
}

func New(bindPort int, bindHost, clientId, clientSecret, scopes string) (*Config, error) {
	if clientId == "" {
		return nil, errors.New("client_id is a required field")
	}

	if clientSecret == "" {
		return nil, errors.New("client_secret is a required field")
	}

	return &Config{
		BindPort:            bindPort,
		BindHost:            bindHost,
		SpotifyClientId:     clientId,
		SpotifyClientSecret: clientSecret,
		SpotifyScopes:       scopes,
	}, nil
}
