package middlewares

import (
	"net/http"

	res "gitlab.com/lmendes_94/spotifyauth/server/http"
)

const (
	RequestTypeGet  = "GET"
	RequestTypePost = "POST"
	jsonContentType = "application/json"
)

func withCors(h http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Access-Control-Allow-Origin", "*")
		rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, PUT, DELETE, OPTIONS")
		rw.Header().Set("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Content-length, Accept, x-access-token")
		rw.Header().Set("Access-Control-Allow-Credentials", "true")

		h.ServeHTTP(rw, r)
	}
}

func withRequestType(h http.HandlerFunc, requestType string) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		if r.Method != requestType {
			res.Create(http.StatusMethodNotAllowed, rw, res.SpotifyResponsePayload{"message": "method not allowed"})
			return
		}

		h.ServeHTTP(rw, r)
	}
}

func withAuthentication(h http.HandlerFunc) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		authorization := r.Header.Get("Authorization")

		if authorization == "" {
			res.Create(http.StatusUnauthorized, rw, res.SpotifyResponsePayload{
				"message": "invalid authorization header",
			})
			return
		}

		h.ServeHTTP(rw, r)
	}
}

func withContentType(h http.HandlerFunc, contentType string) http.HandlerFunc {
	return func(rw http.ResponseWriter, r *http.Request) {
		rw.Header().Set("Content-Type", contentType)
		h.ServeHTTP(rw, r)
	}
}

func HttpRoute(h http.HandlerFunc, requestType string) http.HandlerFunc {
	return withRequestType(withContentType(withCors(h), jsonContentType), requestType)
}

func HttpRouteAuthenticated(h http.HandlerFunc, requestType string) http.HandlerFunc {
	return withAuthentication(withRequestType(withContentType(withCors(h), jsonContentType), requestType))
}
