package server

import (
	"context"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/lmendes_94/spotifyauth/config"
)

type RouteHandler interface {
	Register()
}

type SpotifyAuthServer struct {
	c   *config.Config
	srv *http.Server
}

func (s *SpotifyAuthServer) Start() {
	if err := s.srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatalf("listen: %s\n", err)
	}
}

func (s *SpotifyAuthServer) ShutdownServer(ctx context.Context) error {
	return s.srv.Shutdown(ctx)
}

func setupServerRoutes(handlers ...RouteHandler) {
	for _, handler := range handlers {
		handler.Register()
	}
}

func New(config *config.Config, handlers ...RouteHandler) *SpotifyAuthServer {
	server := &SpotifyAuthServer{
		c: config,
		srv: &http.Server{
			Addr: fmt.Sprintf(":%d", config.BindPort),
		},
	}

	setupServerRoutes(handlers...)
	return server
}
