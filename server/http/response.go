package http

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type SpotifyResponsePayload map[string]interface{}

func (s SpotifyResponsePayload) JSON() string {
	bytes, _ := json.Marshal(s)
	return string(bytes)
}

func Create(status int, w http.ResponseWriter, payload SpotifyResponsePayload) {
	w.WriteHeader(status)
	fmt.Fprintf(w, payload.JSON())
}
